I figure it does not make much sense to update the projects `tecanCavroXLP6000ML`
and `MLberkeley500A` every time a change is made to the `logging` class. Therefore,
a seperate prject has been created for the development of `logging`.