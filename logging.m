classdef logging
    %logging Class for printing output onto screen and into log file.
    %   Provides a set of functions for printing output onto the screen
    %   and/or into a log file.
    %
    %   logging(filePath, fileName) defines the location and filename of
    %   the log file. This command is optional. Default is file
    %   `logFile.log` in the current directory.
    %
    %   The path and filename of the log file is passed between the
    %   functions as a global variable `loggingObj`. All screen output will
    %   be appended to the same file until another file is declared or the
    %   global variable is deleted.
    
    properties
        filePath;
        fileName;
    end
    
    methods
        % Class constructor.
        function loggingObj = logging(filePath, fileName)
           global loggingObj;
           if exist('filePath', 'var')
                loggingObj.filePath = filePath;
                if ~exist('filePath', 'dir')
                    mkdir(filePath);
                end
            else
                loggingObj.filePath = pwd;
            end
            if exist('fileName', 'var')
                loggingObj.fileName = fileName;
            else
                loggingObj.fileName = 'logFile.log';
            end            
        end
    end
    
    methods (Static)
        % File header.
        function HEADER(text)
            % Look for loggingObj and create, if necessary (needs to be
            % deleted first, because global logging creates an empty
            % variable of type struct if no existing variable is found).
            global loggingObj;
            if isempty(loggingObj)
                clear global loggingObj;
                loggingObj = logging();
            end
            fid = fopen(fullfile(loggingObj.filePath, loggingObj.fileName), 'a');
            fprintf(fid, '\r\n'); 
            fprintf(fid, '\r\n');
            fprintf(fid, '\r\n');
            fprintf(fid, '******************************** \r\n');
            fprintf(fid, sprintf('%s\r\n', text));
            fprintf(fid, logging.dateString());
            fprintf(fid, '\r\n');
            fprintf(fid, '\r\n');
            fclose(fid);
        end
        
        % List current variables in base workspace.
        function VARS
            logging.printVarsToFile('VARIABLES:');
            vars = evalin('base', 'whos');
            for i = 1:numel(vars)
                % Ignore everything but doubles, strings and logicals.
                if strfind(vars(i).class, 'double')
                    % Ignore doubles with more (or less) than 1 element.
                    if numel(evalin('base', vars(i).name)) == 1
                        text = sprintf('%s = %d', vars(i).name, ...
                                       evalin('base', vars(i).name));
                        logging.printVarsToFile(text);
                        disp(text);
                    end
                elseif strfind(vars(i).class, 'char')
                    text = sprintf('%s = %s', vars(i).name, ...
                                   evalin('base', vars(i).name));
                    % Replace "\" in file path variables by "/" for proper
                    % representation.
                    text = strrep(text, '\', '/');
                    logging.printVarsToFile(text);
                    disp(text);
                elseif strfind(vars(i).class, 'logical')
                    text = sprintf('%s = %d', vars(i).name, ...
                                   evalin('base', vars(i).name));
                    logging.printVarsToFile(text);
                    disp(text);
                end
            end
            % Newline.
            logging.printVarsToFile('\r\n');
        end
            
            
        % Write to screen only.
        function SCREEN(text)
            disp(text);
        end
        
        % Write to file only.
        function FILE(text)
            logging.printToFile(text);
        end
        
        % Write to screen and file.
        function BOTH(text)
            logging.printToFile(text);
            disp(text);
        end
        
        % Warning (screen and file).
        function WARNING(text)
            logging.printToFile(sprintf('WARNING: %s', text));
            warning(text);
        end
        
        % Error (screen and file).
        function ERROR(text)
            logging.printToFile(sprintf('ERROR: %s', text));
            error(text);
        end
        
        % Date-Time string.
        function dateStr = dateString()
            dateStr = datestr(datetime('now'));
        end
        
        % Print to file.
        function printToFile(text)
            % Look for loggingObj and create, if necessary (needs to be
            % deleted first, because global logging creates an empty
            % variable of type struct if no existing variable is found).
            global loggingObj;
            if isempty(loggingObj)
                clear global loggingObj;
                loggingObj = logging();
            end
            % Identify caller function (2 levels up) and create string with
            % name and line.
            callerFunc = dbstack(2);
            funcString = sprintf('%s, line %d', callerFunc(1).name, ...
                                 callerFunc(1).line);
            % Write line.
            fid = fopen(fullfile(loggingObj.filePath, ...
                                 loggingObj.fileName), 'a');
            fprintf(fid, sprintf('%s\t%-35s%s\r\n', ...
                                 logging.dateString(), funcString, text));
            fclose(fid);            
        end
        
         % Print variables to file.
        function printVarsToFile(text)
            % Look for loggingObj and create, if necessary (needs to be
            % deleted first, because global logging creates an empty
            % variable of type struct if no existing variable is found).
            global loggingObj;
            if isempty(loggingObj)
                clear global loggingObj;
                loggingObj = logging();
            end
            % Write line.
            fid = fopen(fullfile(loggingObj.filePath, ...
                                 loggingObj.fileName), 'a');
            fprintf(fid, sprintf('%s\r\n', text));
            fclose(fid);            
        end        
            
    end
            
end
